//////////PRZEKOPIOWAŁEM CAŁY KOD Z POPRZEDNICH ZAJĘĆ//////////////////////

// ZADANIA
// 1. Uruchom moduł Auth w Firestore.
// 2. Utwórz ręcznie użytkownika w konsoli Firestore.
// 3. Dopisz funkcjonalność, która pozwoli na uwierzytelnianie użytkownika poprzez
// adres mailowy oraz hasło w Auth. Wyświetl dane zalogowanego użytkownika.
// 4. Dodaj formularz logowania oraz rejestracji po stronie UI.
// a. Formularz powinien zawierać możliwość wpisania maila oraz hasła.
// 5. Dodaj odpowiednie przyciski logowania, rejestracji oraz wylogowania po stronie UI.
// 6. Dodaj i przetestuj możliwość logowania użytkownika poprzez UI.
// a. Potwierdzniem poprawnego działania tego punktu będzie wyświetlenie w
// konsoli danych zalogowanego użytkownika.
// 7. Dodaj i przetestuj możliwość rejestracji użytkownika poprzez UI.
// a. Potwierdzniem poprawnego działania tego punktu będzie wyświetlenie w
// konsoli danych zarejstrowanego użytkownika.
// 8. Dodaj i przetestuj możliwość wylogowania użytkownika poprzez UI.
// a. Dodatkowo pokazuj formularz logowania/rejestracji tylko jeśli użytkownik nie
// jest zalogowany. Będzie to potwierdzeniem poprawności działania
// wylogowywania.
// 9. Dopisz funkcjonalność, która będzie wyświetlać przycisk Wyloguj tylko jeśli
// użytkownik jest w danym momencie zalogowany.
// 10. Pokaż zalogowanym użytkownikom zawartość kolekcji stworzonej w bazie danych
// w poprzednim module (Firestore Database).
// -> Należy rozpoznać, czy ktoś jest zalogowany
// -> Jeżeli jest to wywołać funkcję, która uzupełni HTML o zawartość kolekcji
// 11. Dodanie możliwości dodawania dokumentu z poziomu UI zalogowanym
// użytkownikom
// -> Należy rozpoznać, czy ktoś jest zalogowany
// -> Należy wyświetlić formularz dodawania nowego zwierzęcia do kolekcji
// stworzonej w poprzednim module. Po naciśnięciu przycisku ‘utwórz’ należy dodać
// rekord do bazy danych oraz odświeżyć listę wszystkich wyświetlanych zwierząt z
// kolekcji z bazy danych.
// -> Skorzystaj z podejścia, w którym ID jest generowane automatycznie przez
// Firebase
// 12. * Dodanie możliwości logowanie poprzez konto Google
// -> https://firebase.google.com/docs/auth/web/google-signin?hl=en

//import funkcji
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js"; //
//import funkcji do autoryzacji
import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,
  signInWithPopup,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js";

import {
  getFirestore,
  doc,
  getDoc,
  setDoc,
  addDoc,
  collection,
  query,
  getDocs,
  where,
  orderBy,
  limit,
  updateDoc,
  deleteDoc,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

//przygotowanie konfiguracji projekt u Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBvIVdYyzCCFHLnhbVgnbWJ49qSBAsGjes",
  authDomain: "serchristof-first-app.firebaseapp.com",
  projectId: "serchristof-first-app",
  storageBucket: "serchristof-first-app.appspot.com",
  messagingSenderId: "513175481572",
  appId: "1:513175481572:web:636b591c516c731c7390f1",
};

//urochomienie aplikacji firebase oraz potrzebnych nam modułów

//-uruchomienie aplikacji
const app = initializeApp(firebaseConfig);

//-uruchomienie modułu uwierzytelnienia
const auth = getAuth(app);

//-uruchomienie modułu bazy danych
const database = getFirestore(app);

//Definiowanie elementów UI - uchwyty
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");

const signUpButton = document.querySelector("#signUpBtn");
const signInButton = document.querySelector("#signInBtn");
const signInWithGoogleBtn = document.querySelector("#signInWithGoogleBtn");
const signOutBtn = document.querySelector("#signOutBtn");
const errorLabel = document.querySelector("#errorLabel");
const content = document.querySelector(".content");
const addNewDoc = document.querySelector(".addNewDoc");
const animalNameForm = document.querySelector("#animalNameForm");
const createAnimalBtn = document.querySelector("#createAnimalBtn");

const viewForNotLoggedUser = document.querySelector("#viewForNotLoggedUser");
const viewForLoggedUser = document.querySelector("#viewForLoggedUser");
const headerH1 = document.querySelector("h1");

////////////////////////////OPERACJA 1: zalogowanie użytkownika////////////////////

const signInUser = async () => {
  const valueEmail = emailForm.value;
  const valuePassword = passwordForm.value;

  //obsługa błędu - jakbyśmy tego nie dali to przy błędnym haśle lub loginie wysypał by się program
  try {
    //signInWithEmailAndPassword(moduł_auth,email_usera,haslo_usera)
    const user = await signInWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    //exception - parametr opcjonalny, generuje w consoli więcej info o błędzie
    //errory - te kody błędów są ustalone przez firebase i możesz z nich skorzystać są w dokumentaji
  } catch (error) {
    if (error.code === "auth/email-already-in-use") {
      console.log("wybierz inny email, ten jest zajęty..");
    } else {
      console.log("Rejestracja nie powiodła się...");
    }
  }
};

//dodanie eventu do przyisku SignIn (wywołanie funkcji logującej użytkownika)
signInButton.addEventListener("click", signInUser);

////////////////////////OPERACJA 2: REJESTRACJA UŻYTKOWNIKA//////////////////////

const signUpUser = async () => {
  const valueEmail = emailForm.value;
  const valuePassword = passwordForm.value;
  try {
    const user = await createUserWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log("Opracja dodania użytkownika powiodła się");
  } catch (error) {
    console.log(error.code);
    console.log("rejestracja sie nie powiodła");
  }

  //WAŻNE
  //Dodatnie do bazy danych nowego użytkownika (np.firestore)
  //wskaźnik - >doc(database,'użytkownik',user.user.uid
};

//Dodanie eventu do przycisku signUp() wywołanie funkcji rejestrującej
signUpButton.addEventListener("click", signUpUser);

////////////////////////////OPERACJA 3 - WYLOGOWANIE UŻYTKOWNIKA///////////////////////

const signOutUser = async () => {
  await signOut(auth);
  console.log("User log out");
  emailForm.value = "";
  passwordForm.value = "";
};

signOutBtn.addEventListener("click", signOutUser);

//////////////////OPERACJA 4- STATUS SESJI - NASŁUCHIWANIE NA ZMIANĘ//////////////////////////
const authUserObserver = () => {
  //przyjmuje dwa parametry -
  onAuthStateChanged(auth, (user) => {
    if (user) {
      //ktoś jest zalogowany
      viewForLoggedUser.style.display = "block";
      viewForNotLoggedUser.style.display = "none";
      headerH1.innerHTML = "Hej zalogowałeś się";

      //ZADANIE 10.

      showCollectionAnimals();
      createAnimal();
    } else {
      //ktoś nie jest zalogowany
      viewForLoggedUser.style.display = "none";
      viewForNotLoggedUser.style.display = "block";
      headerH1.innerHTML = "Zaloguj się:";
    }
  });
};

authUserObserver();

//ZADANIE 10, POKAŻ KOLEKCJE ZALOGOWANEMU UŻYTKOWNIKOWI
const showCollectionAnimals = async () => {
  const animalCollectionRef = query(collection(database, "Zwierzęta"));
  const allAnimalsSnap = await getDocs(animalCollectionRef); //poczekaj (await) jak dostaniesz dane z query
  content.innerHTML = "<ol>";
  allAnimalsSnap.forEach((animal) => {
    content.innerHTML += `<li>${animal.data().nazwa}</li>`;
  });
  content.innerHTML += "</ol>";
};

//11. DODAJ ZWIERZAKA Z POZIOMU UI

const createAnimal = async () => {
  const name = animalNameForm.value;
  const animalsCollectionRef = collection(database, "zwierzeta");

  await addDoc(animalsCollectionRef, { nazwa: name });
};

//ZADANIE 12:

const signInWithGoogle = () => {
  const authProvider = new GoogleAuthProvider();
  signInWithPopup(auth, authProvider);
};
signInWithGoogleBtn.addEventListener("click", signInWithGoogle);
