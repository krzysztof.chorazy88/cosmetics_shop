//----------------------------------2023.02.18---------------------------------------//

// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

//Import modułu STORAGE
import {
  getStorage,
  uploadBytesResumable,
  ref,
  getDownloadURL,
  listAll,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-storage.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
  apiKey: "AIzaSyBvIVdYyzCCFHLnhbVgnbWJ49qSBAsGjes",
  authDomain: "serchristof-first-app.firebaseapp.com",
  projectId: "serchristof-first-app",
  storageBucket: "serchristof-first-app.appspot.com",
  messagingSenderId: "513175481572",
  appId: "1:513175481572:web:636b591c516c731c7390f1",
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);

//- uruchomienie modułu storage
const storage = getStorage(app);

// - definiowanie elementów UI
const imageInput = document.querySelector("#imageInput");
const imageName = document.querySelector("#imageName");
const uploadBtn = document.querySelector("#uploadBtn");
const uploadImageProgressBar = document.querySelector(
  "#uploadImageProgressBar"
);
const uploadedImage = document.querySelector("#uploadedImage");

//do pkt. 7
const imagesDropDownList = document.querySelector("#imagesDropDownList");
const selectedImage = document.querySelector("#selectedImage");
const audioPlayer = document.querySelector("#audioPlayer");

//--------------------WRZUCANIE PLIKU Z DYSKU LOKALNEGO NA SERWER----------------------//
// 1. Zmienne pomocnicze

let fileToUpload;
let fileExtension;
let validate = true; //sprawdza czy plik posiada dozwolone znaki

// 2. Obsługa procesu obsługi pliku

// 2.1 Wbijamy się w moment kiedy plik zostanie wybrany, więc event 'change' zostanie wywołany
//w każdym zdarzeniu jest dostępny parametr event który możemy wykorzystać
imageInput.onchange = (event) => {
  // 2.2 Zapamiętanie całego pliku, któ©y wrzucimy na Storage
  fileToUpload = event.target.files[0]; // ta ścieżka jest do odczytu ze zdarzenia w consli - tu cały plik jako element tablicy przypisujemy do zmiennej
  console.log(event);

  //2.3 Pobranie pełnej nazwy wybranego przez użytkownika pliku
  const fullFileName = event.target.files[0].name;

  //2.4 Podział nazwy wybranego pliku na nazwę oraz rozszerzenie
  //[nazwa_pliku, rozszerzenie]

  const splittedFileName = fullFileName.split(".");

  //6. Dodaj zabezpieczenie przed nazwami plików zawierającymi “.” w środku nazwy,
  // np. a.b.c.jpg.

  if (splittedFileName.length > 2) {
    console.log("plik posiada nie dozwolone znaki");
    validate = false;
  } else {
    //2.5 Pobranie samego rozszerzenia pliku
    fileExtension = splittedFileName[splittedFileName.length - 1];
  }
  console.log(fileExtension);
};

// 3. Przesyłanie wybranego obrazka do Firebase(Storage)
uploadBtn.onclick = () => {
  if (validate) {
    //3.1 Pobranie nazwy docelowego pliku z inputa
    const fileName = imageName.value;

    //3.2 Złączenie docelowej nazwy pliku z roszerzeniem pliku
    //Plik musi mieć pełną nazwę tzn. nazwa + rozszerzenie
    const fullFileName = `${fileName}.${fileExtension}`;

    //3.3 Określenie metadanych
    const metaData = {
      contentType: fileToUpload.type,
    };

    //3.4 Wrzucenie pliku na serwer - komunikacja ze storage
    // uploadBytesResumable(ref(jaki_moduł?,w jakim folderze?, jaki plik?, wskaż metadane))

    const uploadProcess = uploadBytesResumable(
      ref(storage, `images/${fullFileName}`),
      fileToUpload,
      metaData
    );
    //3.5 wbijamy się w moment, kiedy status wrzucania pliku będzie się zmieniał
    //skłądnia callback
    uploadProcess.on(
      "state_changed",

      //1.funkcja - co robić w trakcie
      //snapshot - stan wysyłania w danym momencie
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        uploadImageProgressBar.innerHTML = Math.round(progress) + "%";
      },

      //2.funkcja - co robić kiedy funkcja zakońćzy się błędem
      (error) => {
        console.log(error);
      },
      //3.funkcja - co kiedy proces zakończy się sukcesem
      async () => {
        //3.6 Pobranie URL pliku
        const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);

        //3.7 Umieszczenie obrazka na stronie
        console.log(uploadProcess.snapshot.ref);
        uploadedImage.setAttribute("src", imageUrl); //setAttribute - dodaje do html atrybut dla <img>
      }
    );
  } else {
    alert("Plik ma nieprawidłową nazwę");
  }
};

// -------------------ODCZYT PLIKÓW Z SERWERA-----------------------------------//
// 4. Pobranie konkretnego obrazka

//4.1 Pobranie URL obrazka
const imageUrl = await getDownloadURL(ref(storage, "images/nazwa_pliku.jpg")); //    TU WPISZ SWOJĄ ŚCIEŻKĘ PLIKU !!!!!!!!

//4.2 Umieszczenie obrazka na stronie
uploadedImage.setAttribute("src", imageUrl);

// 5. Pobranie wszystkich dostępnych plików (w folderze)
//5.1 Pobieranie referencji (wskaźników) plików z konkretnego
//folderu. Jako wynik otrzymamy tablicę

const allImages = await listAll(ref(storage, "images")); //listAll jest asynchroniczne

//5.2 przejście pętlą przez wszystkie znalezione pliki
// allImages.items.forEach(async (imageRef) => {
//   const imageUrl = await getDownloadURL(imageRef);
//   //wyświetlanie linków do zdjęć -> console.log(imageUrl)

//   //wyświetlanie ścieżek zdjęć:
//   console.log(imageRef._location.path);
// });

//------------------------------MEGA WAŻNE------------------------------//
//5.2. NOWE - UWZGLĘDNIONY PROMISE
//Przejście pętlą przez wszystkie znalezione pliki - map()
//TU OSTRA ZABAWA Z SYNCHRONICZNOŚCIĄ
//SPRAWDŹ ODNIESIENIA BO COŚ NIE DZIAŁĄ
let allImagesView = "<ul>";

// [ref1, ref2] --> [obietnica, ze dostane link1; obietnica, ze dostane link2]

// a) kod leci dalej ale pod spodem tworzony jest Promis który czeka aż pobrane zostaną linki do obrazków
//ze storage
const getAllImagesPromise = allImages.items.map(async (imageRef) => {
  const imageUrl = await getDownloadURL(imageRef);
  return `<li><img src="${imageUrl}"></li>`;
});

//b) a tu do listy wrzucamy załapane promisty z kroku a)
// i forEach() dodajemy elementy html owe (on tu wrzucił poprostu na stronę obrazki)
const arrayWithImagesListElements = await Promise.all(getAllImagesPromise);
arrayWithImagesListElements.forEach((imageListElement) => {
  allImagesView += imageListElement;
});

allImagesView += "</ul>";

uploadImageProgressBar.innerHTML = allImagesView;

//------------------------------- Storage:ZADANIA, 2023.12.19----------------------------//

// 1. Uruchom moduł Storage w Firebase.
// 2. Dodaj ręcznie dowolny obrazek do Storage’a (zapisz go w folderze głównym).
// 3. Utwórz folder ‘images’, w którym będziesz przechowywać obrazki w ramach
// Storage’a.
// 4. Dodaj funkcjonalność, która pozwoli użytkownikowani na dodanie obrazka do
// Storage’a:
// a. Pozwól wybrać użytkownikowi obrazek z dysku lokalnego.
// b. Automatycznie pobieraj obiekt pliku oraz rozszerzenie przesłanego pliku.
// c. Pozwól użytkownikowi na zmianę nazwy pliku przed wrzuceniem na Storage.
// d. Dodaj przycisk, który rozpocznie proces wrzucania pliku do Storage.
// e. W momencie, kiedy przesyłanie się zakończy - wyświetl wrzucony obrazek.
// 5. Dodaj funkcjonalność, która pozwoli wyświetli w UI wszystkie pliki znajdujące się w
// folderze ‘images’ na Storage.
// 6. ** Dodaj zabezpieczenie przed nazwami plików zawierającymi “.” w środku nazwy,
// np. a.b.c.jpg.
// 7. ** Z pobranej listy wszystkich plików zrobić drop-down list.
// a. Wyświetl wybrany obrazek z drop-down listy.
// b. Dodaj możliwość usuwania obrazków z Storage’a.
// 8. ** Dodaj możliwość wrzucenia pliku audio oraz odsłuchania tego pliku.
// 9. *** Ustaw zasady bezpieczeństwa dla modułu Storage:

//------------------OD 6. BO TO NOWE RZECZY-------------------------------//

// Zadanie 7a
// 7.1: Pobranie wszystkich obrazków
//LINIJKA PONIŻEJ JEST TOŻSAMA Z PKT. 5.1
const allImages = await listAll(ref(storage, 'images'));

// 7.2: Utworzenie drop down listy -> HTML
let imagesDropDownListHTML = `<select id="allImagesSelectList">`;

// 7.3: Wrzucenie do listy rozwijanej nazw plików -> HTML
allImages.items.forEach((imageRef) => {
  // Pobranie nazw wszystkich plików
  const imageName = imageRef.name.split(".")[0];

  // Wygenerowanie option dla każdego z obrazków
  imagesDropDownListHTML += `<option value="${imageRef.fullPath}">${imageName}</option>`;
});

// 7.4: Zamknięcie listy rozwijanej -> HTML
imagesDropDownListHTML += "</select>";

// 7.5: Umieszczenie listy rozwijanej na stronie
imagesDropDownList.innerHTML = imagesDropDownListHTML;

// 7.6: Dodanie reagowania na zmiany w rozwijaku
imagesDropDownList.onchange = async (event) => {
  // 7.6.1: Pobranie obrazku ze storage na bazie pełnej ścieżki
  const selectedImageFullPath = event.target.value;
  const imageUrl = await getDownloadURL(ref(storage, selectedImageFullPath));
  selectedImage.setAttribute("src", imageUrl);
};

// 8.1. Pobranie linku do pliku audio ze Storage'a
const audioUrl = await getDownloadURL(ref(storage, "audio/audio.mp3"));

// 8.2. Umieszczenie na stronie odtwarzacza audio -> HTML
// 8.3. Podpięcie pod odtwarzacz pobranego pliku audio
let audioPlayerHTML = `<audio controls>
        <source src="${audioUrl}" type="audio/mpeg">
     </audio>
    `;

// 8.4. Umieszczenie odtwarzacza na stronie
audioPlayer.innerHTML = audioPlayerHTML;
