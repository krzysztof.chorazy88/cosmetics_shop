//----------------------------------REALTIME DATABESE--------------------------------//
//------------------------------------------------------------------------------------//

// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

//REALTIME DATABASE
import {
  getDatabase,
  push,
  ref,
  onValue,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyBvIVdYyzCCFHLnhbVgnbWJ49qSBAsGjes",
  authDomain: "serchristof-first-app.firebaseapp.com",
  databaseURL:
    "https://serchristof-first-app-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "serchristof-first-app",
  storageBucket: "serchristof-first-app.appspot.com",
  messagingSenderId: "513175481572",
  appId: "1:513175481572:web:636b591c516c731c7390f1",
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);

//- uruchomienie modułu realtime database
const database = getDatabase(app);

//DEFINIOWANIE ELEMENTÓW UI

// Definiowanie elementów UI
const nameInput = document.querySelector("#nameInput");
const messageInput = document.querySelector("#messageInput");
const sendMessageBtn = document.querySelector("#sendMessageBtn");
const chatArea = document.querySelector("#chatArea");

//1. Dodanie możliwości wysyłania wiadomości

const sendMessage = () => {
  //1.1 Pobranie imienia użytkownika z inputa
  const authorName = nameInput.value;
  //1.2 Pobranie wiadomości z inputa
  const messageName = messageInput.value;

  //1.3 Zapisanie imienia oraz wiadomości w bazie danych
  //push ->dodanie do bazy z automatycznie nadanym kluczem
  //push(gdzie?, co(jaki obiekt)? )
  push(ref(database, "messages"), {
    //tworzy sie folder messages w Realtime Database
    author: authorName,
    message: messageName,
  });
};
//1.4 Podpięcie pod przycisk wysyłania danych do bazy

sendMessageBtn.addEventListener("click", sendMessage);

//2. Dodanie możliwości odczytu wiadomości
//2.1 Dodanie nałuchiwania na zmiany pod ścieżką 'message'
//onValue(na co ma nasłuchiwać?, co robić jak się cos zmieni?)
//za każdym razem jak się coś zmieni coś w 'messages' to robi się nowy snapshot
onValue(ref(database, "messages"), (snapshot) => {
  //2.2 Wygenerowanie widoku HTML ze wszystkimi wiadomościami
  let chatContent = "";
  Object.values(snapshot.val()).forEach((message) => {
    chatContent += `${message.author}: ${message.message}<br>`;
  });
  //2.3 Umieszczenie wiadomości na stronie
  chatArea.innerHTML = chatContent;
});
